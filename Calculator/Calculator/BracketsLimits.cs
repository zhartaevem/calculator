﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    public class BracketsLimits
    {
        public int BracketsSkipLimit { get; set; }
        public int AnotherElemSkipLimit { get; set; }

        public BracketsLimits(bool isFirstCalculate)
        {
            this.BracketsSkipLimit = isFirstCalculate ? 1 : 2;

            this.AnotherElemSkipLimit = isFirstCalculate ? 0 : 1;
        }
    }
}
