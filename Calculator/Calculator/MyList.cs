﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Contracts;
using System.Text;

namespace Calculator
{
    public class MyList<T> : IList<T>
    {
        private T[] _myItems;

        private int _size;
        private int _version;
        private Object _syncRoot;

        static readonly T[] _emptyArray = new T[0];

        private const int _defaultCapacity = 4;

        internal const int MaxArrayLength = 0X7FEFFFFF;

        public MyList()
        {
            _myItems = _emptyArray;
        }


        public MyList(int capacity)
        {
            if (capacity < 0) throw new IndexOutOfRangeException();

            if (capacity == 0)
                _myItems = _emptyArray;
            else
                _myItems = new T[capacity];
        }

        public MyList(IEnumerable<T> collection)
        {
            if (collection == null) throw new NullReferenceException();
            Contract.EndContractBlock();

            ICollection<T> c = collection as ICollection<T>;
            if (c != null)
            {
                int count = c.Count;
                if (count == 0)
                {
                    _myItems = _emptyArray;
                }
                else
                {
                    _myItems = new T[count];
                    c.CopyTo(_myItems, 0);
                    _size = count;
                }
            }
            else
            {
                _size = 0;
                _myItems = _emptyArray;

                using (IEnumerator<T> en = collection.GetEnumerator())
                {
                    while (en.MoveNext())
                    {
                        Add(en.Current);
                    }
                }
            }
        }

        public int Capacity
        {
            get
            {
                return _myItems.Length;
            }
            set
            {
                if (value < _size)
                {
                    throw new IndexOutOfRangeException();
                }

                if (value != _myItems.Length)
                {
                    if (value > 0)
                    {
                        T[] newItems = new T[value];
                        if (_size > 0)
                        {
                            Array.Copy(_myItems, 0, newItems, 0, _size);
                        }

                        _myItems = newItems;
                    }
                    else
                    {
                        _myItems = _emptyArray;
                    }
                }
            }
        }

        public int Count
        {
            get
            {
                return _size;
            }
        }

        bool IsFixedSize
        {
            get { return false; }
        }


        bool ICollection<T>.IsReadOnly
        {
            get { return false; }
        }

        bool IsReadOnly
        {
            get { return false; }
        }

        bool IsSynchronized
        {
            get { return false; }
        }

        Object SyncRoot
        {
            get
            {
                if (_syncRoot == null)
                {
                    System.Threading.Interlocked.CompareExchange<Object>(ref _syncRoot, new Object(), null);
                }

                return _syncRoot;
            }
        }

        public T this[int index]
        {
            get
            {
                if ((uint) index >= (uint) _size)
                {
                    throw new ArgumentOutOfRangeException();
                }

                return _myItems[index];
            }

            set
            {
                if ((uint) index >= (uint) _size)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _myItems[index] = value;
                _version++;
            }
        }

        private static bool IsCompatibleObject(object value)
        {
            return ((value is T) || (value == null && default(T) == null));
        }

        public void Add(T item)
        {
            if (_size == _myItems.Length) EnsureCapacity(_size + 1);
            _myItems[_size++] = item;
            _version++;
        }

        int Add(Object item)
        {
            if (item == null && !(default(T) == null))
            {
                throw new ArgumentNullException();
            }

            try
            {
                Add((T) item);
            }
            catch (InvalidCastException)
            {
                throw new InvalidCastException();
            }

            return Count - 1;
        }

        public ReadOnlyCollection<T> AsReadOnly()
        {
            return new ReadOnlyCollection<T>(this);
        }

        public int BinarySearch(int index, int count, T item, IComparer<T> comparer)
        {
            if (index < 0)
                new ArgumentOutOfRangeException();
            if (count < 0)
                new ArgumentOutOfRangeException();
            if (_size - index < count)
                new ArgumentException();

            return Array.BinarySearch<T>(_myItems, index, count, item, comparer);
        }

        public int BinarySearch(T item)
        {
            return BinarySearch(0, Count, item, null);
        }

        public int BinarySearch(T item, IComparer<T> comparer)
        {
            return BinarySearch(0, Count, item, comparer);
        }

        public void Clear()
        {
            if (_size > 0)
            {
                Array.Clear((_myItems), 0, _size);
                _size = 0;
            }

            _version++;
        }

        public bool Contains(T item)
        {
            if ((Object) item == null)
            {
                for (int i = 0; i < _size; i++)
                    if ((Object) _myItems[i] == null)
                        return true;
                return false;
            }
            else
            {
                EqualityComparer<T> c = EqualityComparer<T>.Default;
                for (int i = 0; i < _size; i++)
                {
                    if (c.Equals(_myItems[i], item)) return true;
                }

                return false;
            }
        }

        bool Contains(Object item)
        {
            if (IsCompatibleObject(item))
            {
                return Contains((T) item);
            }

            return false;
        }

        public MyList<TOutput> ConvertAll<TOutput>(Converter<T, TOutput> converter)
        {
            if (converter == null)
            {
                throw new ArgumentNullException();
            }

            MyList <TOutput> list = new MyList<TOutput>(_size);
            for (int i = 0; i < _size; i++)
            {
                list._myItems[i] = converter(_myItems[i]);
            }

            list._size = _size;
            return list;
        }

        public void CopyTo(T[] array)
        {
            CopyTo(array, 0);
        }

        void CopyTo(Array array, int arrayIndex)
        {
            if ((array != null) && (array.Rank != 1))
            {
                throw new ArgumentException();
            }

            try
            {
                Array.Copy(_myItems, 0, array, arrayIndex, _size);
            }
            catch (ArrayTypeMismatchException)
            {
                throw new ArgumentException();
            }
        }

        public void CopyTo(int index, T[] array, int arrayIndex, int count)
        {
            if (_size - index < count)
            {
                throw new ArgumentException();
            }

            Array.Copy(_myItems, index, array, arrayIndex, count);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Array.Copy(_myItems, 0, array, arrayIndex, _size);
        }

        private void EnsureCapacity(int min)
        {
            if (_myItems.Length < min)
            {
                int newCapacity = _myItems.Length == 0 ? _defaultCapacity : _myItems.Length * 2;

                if ((uint) newCapacity > MaxArrayLength) newCapacity = MaxArrayLength;
                if (newCapacity < min) newCapacity = min;
                Capacity = newCapacity;
            }
        }

        public bool Exists(Predicate<T> match)
        {
            return FindIndex(match) != -1;
        }

        public T Find(Predicate<T> match)
        {
            if (match == null)
            {
                throw new ArgumentNullException();
            }

            for (int i = 0; i < _size; i++)
            {
                if (match(_myItems[i]))
                {
                    return _myItems[i];
                }
            }

            return default(T);
        }

        public List<T> FindAll(Predicate<T> match)
        {
            if (match == null)
            {
                throw new ArgumentNullException();
            }

            List<T> list = new List<T>();
            for (int i = 0; i < _size; i++)
            {
                if (match(_myItems[i]))
                {
                    list.Add(_myItems[i]);
                }
            }

            return list;
        }

        public int FindIndex(Predicate<T> match)
        {
            return FindIndex(0, _size, match);
        }

        public int FindIndex(int startIndex, Predicate<T> match)
        {
            return FindIndex(startIndex, _size - startIndex, match);
        }

        public int FindIndex(int startIndex, int count, Predicate<T> match)
        {
            if ((uint) startIndex > (uint) _size)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (count < 0 || startIndex > _size - count)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (match == null)
            {
                throw new ArgumentNullException();
            }

            int endIndex = startIndex + count;
            for (int i = startIndex; i < endIndex; i++)
            {
                if (match(_myItems[i])) return i;
            }

            return -1;
        }

        public T FindLast(Predicate<T> match)
        {
            if (match == null)
            {
                throw new ArgumentNullException();
            }

            for (int i = _size - 1; i >= 0; i--)
            {
                if (match(_myItems[i]))
                {
                    return _myItems[i];
                }
            }

            return default(T);
        }

        public int FindLastIndex(Predicate<T> match)
        {
            return FindLastIndex(_size - 1, _size, match);
        }

        public int FindLastIndex(int startIndex, Predicate<T> match)
        {
            return FindLastIndex(startIndex, startIndex + 1, match);
        }

        public int FindLastIndex(int startIndex, int count, Predicate<T> match)
        {
            if (match == null)
            {
                throw new ArgumentNullException();
            }

            if (_size == 0)
            {
                if (startIndex != -1)
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
            else
            {
                if ((uint) startIndex >= (uint) _size)
                {
                    throw new ArgumentOutOfRangeException();
                }
            }

            if (count < 0 || startIndex - count + 1 < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            int endIndex = startIndex - count;
            for (int i = startIndex; i > endIndex; i--)
            {
                if (match(_myItems[i]))
                {
                    return i;
                }
            }

            return -1;
        }

        public bool Remove(T item)
        {
            int index = IndexOf(item);
            if (index >= 0)
            {
                RemoveAt(index);
                return true;
            }

            return false;
        }

        public void RemoveAt(int index)
        {
            if ((uint)index >= (uint)_size)
            {
                throw new ArgumentOutOfRangeException();
            }
            Contract.EndContractBlock();
            _size--;
            if (index < _size)
            {
                Array.Copy(_myItems, index + 1, _myItems, index, _size - index);
            }
            _myItems[_size] = default(T);
            _version++;
        }

        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return new Enumerator(this);
        }

        public MyList<T> GetRange(int index, int count)
        {
            if (index < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (count < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (_size - index < count)
            {
                throw new ArgumentException();
            }

            MyList<T> list = new MyList<T>(count);
            Array.Copy(_myItems, index, list._myItems, 0, count);
            list._size = count;
            return list;
        }

        public int IndexOf(T item)
        {
            return Array.IndexOf(_myItems, item, 0, _size);
        }

        int IndexOf(Object item)
        {
            if (IsCompatibleObject(item))
            {
                return IndexOf((T) item);
            }

            return -1;
        }

        public int IndexOf(T item, int index)
        {
            if (index > _size)
                throw new ArgumentNullException();
            return Array.IndexOf(_myItems, item, index, _size - index);
        }

        public int IndexOf(T item, int index, int count)
        {
            if (index > _size)
                throw new ArgumentOutOfRangeException();

            if (count < 0 || index > _size - count)
                throw new ArgumentOutOfRangeException();

            return Array.IndexOf(_myItems, item, index, count);
        }

        public void Insert(int index, T item)
        {
            if ((uint) index > (uint) _size)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (_size == _myItems.Length) EnsureCapacity(_size + 1);
            if (index < _size)
            {
                Array.Copy(_myItems, index, _myItems, index + 1, _size - index);
            }

            _myItems[index] = item;
            _size++;
            _version++;
        }

        void Remove(Object item)
        {
            if (IsCompatibleObject(item))
            {
                Remove((T) item);
            }
        }

        public int RemoveAll(Predicate<T> match)
        {
            if (match == null)
            {
               throw new ArgumentNullException();
            }

            int freeIndex = 0;

            while (freeIndex < _size && !match(_myItems[freeIndex])) freeIndex++;
            if (freeIndex >= _size) return 0;

            int current = freeIndex + 1;
            while (current < _size)
            {
                while (current < _size && match(_myItems[current])) current++;

                if (current < _size)
                {
                    _myItems[freeIndex++] = _myItems[current++];
                }
            }

            Array.Clear(_myItems, freeIndex, _size - freeIndex);
            int result = _size - freeIndex;
            _size = freeIndex;
            _version++;
            return result;
        }

        public T[] ToArray()
        {
            T[] array = new T[_size];
            Array.Copy(_myItems, 0, array, 0, _size);
            return array;
        }

        public struct Enumerator : IEnumerator<T>, System.Collections.IEnumerator
        {
            private MyList<T> list;
            private int index;
            private int version;
            private T current;

            internal Enumerator(MyList<T> list)
            {
                this.list = list;
                index = 0;
                version = list._version;
                current = default(T);
            }

            public void Dispose()
            {
            }

            public bool MoveNext()
            {

                MyList<T> localList = list;

                if (version == localList._version && ((uint)index < (uint)localList._size))
                {
                    current = localList._myItems[index];
                    index++;
                    return true;
                }
                return MoveNextRare();
            }

            private bool MoveNextRare()
            {
                if (version != list._version)
                {
                    throw new InvalidOperationException();
                }

                index = list._size + 1;
                current = default(T);
                return false;
            }

            public T Current
            {
                get
                {
                    return current;
                }
            }

            Object System.Collections.IEnumerator.Current
            {
                get
                {
                    if (index == 0 || index == list._size + 1)
                    {
                        throw new InvalidOperationException();
                    }
                    return Current;
                }
            }

            void System.Collections.IEnumerator.Reset()
            {
                if (version != list._version)
                {
                    throw new InvalidOperationException();
                }

                index = 0;
                current = default(T);
            }
        }
    }

}
