﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Calculator
{
    public class Calculator
    {
        private string _input { get; set; }

        private List<string> _normalizedData { get; set; }

        private string _errorMessage { get; set; }

        public Calculator()
        {
        }


        public string Launch(string userInput)
        {
            this._input = userInput;

            try
            {
                if (!PrimaryValidate())
                {
                    return _errorMessage;
                }

                this._input = this._input.Replace(",", ".");

                this._normalizedData = Regex.Matches(_input, @"\d+(?:\.\d+)?|[(+-\/*)]").Select(m => m.Value).ToList();

                if (!BracketsValidate())
                {
                    return _errorMessage;
                }

                var result = Calculate(this._normalizedData, true);

                return result ?? _errorMessage;
            }
            catch (Exception e)
            {
                return "Syntax Error";
            }
        }


        private bool PrimaryValidate()
        {
            if (String.IsNullOrEmpty(_input))
            {
                this._errorMessage = "Type input data, please";

                return false;
            }

            return true;
        }


        private bool BracketsValidate()
        {
            bool isValid = true;

            Stack<string> stack = new Stack<string>();

            foreach (var item in this._normalizedData)
            {
                if (item == "(")
                {
                    stack.Push(item);
                }
                else if (item == ")")
                {
                    if (!stack.Any() || stack.Pop() != "(")
                    {
                        isValid = false;
                        this._errorMessage = "Brackets Error";
                        break;
                    }
                }
            }

            return isValid;
        }


        private string Calculate(List<string> input, bool isFirstCalculate = false)
        {
            var bracketsCount = isFirstCalculate ? 0 : 1;

            var bracketsLimits = new BracketsLimits(isFirstCalculate);

            var condition = GetCondition(isFirstCalculate);

            List<string> expression = new List<string>();

            for (var i = 0; condition(i, input.Count, bracketsCount); i++)
            {
                if (input[i] == "(")
                {
                    bracketsCount++;

                    if (bracketsCount > bracketsLimits.BracketsSkipLimit)
                    {
                        continue;
                    }

                    expression.Add(Calculate(input.Skip(i + 1).ToList()));

                }
                else if (input[i] == ")")
                {
                    bracketsCount--;
                }
                else
                {
                    if (bracketsCount > bracketsLimits.AnotherElemSkipLimit)
                    {
                        continue;
                    }

                    if (i == 0 || input[i - 1] == "(")
                    {
                        if (input[i] == "+" || input[i] == "-")
                        {
                            expression.Add("0");
                        }
                        else if (input[i] == "*" || input[i] == "/")
                        {
                            throw new Exception();
                            //return null;
                        }
                    }

                    expression.Add(input[i]);
                }
            }

            return CalculateSimpleArithmetic(expression);
        }


        private Func<int, int, int, bool> GetCondition(bool isFirstCalculate)
        {
            if (isFirstCalculate)
            {
                return (int i, int count, int bracketsCount) => i < count;
            }

            return (int i, int count, int bracketsCount) => i < count && bracketsCount > 0;
        }


        private string CalculateSimpleArithmetic(List<string> input)
        {
            var mainCalculationResult = CalculateMainActions(input);

            return CalculateMinorActions(mainCalculationResult);
        }


        private List<string> CalculateMainActions(List<string> input)
        {
            List<string> resultCalculation = new List<string>();

            string previousOperand = input[0];

            for (var i = 0; i < input.Count(); i++)
            {
                if (input[i] == "*" || input[i] == "/")
                {
                    var result = SimpleCalculate(previousOperand, input[i + 1], input[i]);
                    resultCalculation.RemoveAt(resultCalculation.Count - 1);
                    resultCalculation.Add(result);
                    previousOperand = result;
                    i++;
                }
                else if (input[i] == "+" || input[i] == "-")
                {
                    previousOperand = null;
                    resultCalculation.Add(input[i]);
                }
                else
                {
                    resultCalculation.Add(input[i]);
                    previousOperand = input[i];
                }
            }

            return resultCalculation;
        }


        private string CalculateMinorActions(List<string> input)
        {
            string calculationResult = input[0];

            for (var i = 1; i < input.Count; i++)
            {
                if (input[i] == "+" || input[i] == "-")
                {
                    calculationResult = SimpleCalculate(calculationResult, input[i + 1], input[i]);
                    i++;
                }
                else
                {
                    throw new Exception();
                    //return null;
                }
            }

            return calculationResult;
        }

        private string SimpleCalculate(string firstOperand, string secondOperand, string operation)
        {
            if (!double.TryParse(firstOperand, NumberStyles.Any, CultureInfo.InvariantCulture, out var first) || !double.TryParse(secondOperand, NumberStyles.Any, CultureInfo.InvariantCulture, out var second))
            {
                throw new Exception();
                //return null;
            }

            switch (operation)
            {
                case "+":
                {
                    return (first + second).ToString();
                }

                case "-":
                {
                    return (first - second).ToString();
                }

                case "*":
                {
                    return (first * second).ToString();
                }

                case "/":
                {
                    return (first / second).ToString();
                }

                default:
                    throw new Exception();
                    //return null;
            }
        }

    }
}
