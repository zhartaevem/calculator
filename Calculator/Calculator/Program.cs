﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("-> Calculate ");

            bool calculatorOn = true;

            var calculator = new Calculator();

            while (calculatorOn)
            {
                calculatorOn = CalculatorOn(calculator);
            }

            Console.WriteLine("Press any key to exit");

            Console.ReadKey();
        }

        private static bool CalculatorOn(Calculator calculator)
        {
            var userInput = Console.ReadLine();

            if (String.IsNullOrEmpty(userInput))
            {
                Console.WriteLine("Type input data, please");

                return true;
            }

            if (userInput.ToLower() == "exit")
            {
                return false;
            }

            Calculate(userInput, calculator);

            return true;
        }


        public static void Calculate(string input, Calculator calculator)
        {
            input = input.Replace(",", ".");

            var result = calculator.Launch(input);

            Console.WriteLine("Result: {0}", result);

            Console.WriteLine("\nType 'Exit' to leave :( Or try another calculation :)\n");
        }
    }
}
