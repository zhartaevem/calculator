using System;
using Xunit;

namespace Calculator.Tests
{
    public class CalculatorTests
    {
        [Fact]
        public void CalculateSimpleIntExpressionTest()
        {
            // Arrange
            const string input = "-2 * 3 + 97/1 + 15 - 8";

            var calculator = new Calculator();

            // Act
            var calculationResult = calculator.Launch(input);

            // Assert
            Assert.Equal("98", calculationResult);
        }


        [Fact]
        public void CalculateIntExpressionWithBracketsTest()
        {
            // Arrange
            const string input = "-(2 * (((3 + 97)/10 + 15) - 8))";

            var calculator = new Calculator();

            // Act
            var calculationResult = calculator.Launch(input);

            // Assert
            Assert.Equal("-34", calculationResult);
        }


        [Fact]
        public void CalculateSimpleDoubleExpressionTest()
        {
            // Arrange
            const string input = "-2.99 * 3.578 + 97.8143/11.11111 + 15.988456 - 8.0000001";

            var calculator = new Calculator();

            // Act
            var calculationResult = calculator.Launch(input);

            // Assert
            Assert.Equal("6.093523780328788", calculationResult);
        }
    }
}
